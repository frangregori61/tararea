<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ __('Mis tareas') }}
            </h2>
            <a href="{{route('tasks.create')}}" class="text-lg text-gray-800 dark:text-gray-200 leading-tight bg-blue-400 hover:bg-blue-300 py-2 px-4 rounded-md font-bold transition ease-in-out duration-150">Añadir tarea</a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    {{ __("Bienvenido ") . Auth::user()->name }}
                </div>
                @if ($tasks->isEmpty())
                    <p class="text text-gray-800 dark:text-gray-200 leading-tight text-center pb-6">No hay tareas</p>
                @else
                <p class="text text-gray-800 dark:text-gray-200 leading-tight text-center pb-6">Tienes <strong>{{count($tasks) === 1 ? count($tasks).' tarea' : count($tasks).' tareas'}}</strong></p>

                <ul class="py-6 px-12">
                    @foreach($tasks as $task)
                    <li class="py-4 px-4 mb-10 border rounded-lg shadow text text-gray-800 dark:text-gray-200 leading-tight">
                        <div class="flex justify-between items-center">
                            <div>
                                <h2 class="py-1">{{$task->title}}</h2>
                                <p class="py-1">{{$task->description}}</p>
                            </div>
                            <form method="POST" action="{{ route('tasks.destroy', $task) }}">
                                @csrf
                                @method('DELETE')
                                <x-danger-button class="ms-3">
                                    {{ __('Eliminar') }}
                                </x-danger-button>
                            </form>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </div>
</x-app-layout>
