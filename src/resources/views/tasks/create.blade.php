<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ __('Nueva Tarea') }}
            </h2>

        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-neutral-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <form method="POST" action="{{ route('tasks.store') }}">
                        @csrf
                        <div>
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        </div>

                        <div>
                            <x-input-label for="title" :value="__('Title')" />
                            <x-text-input id="title" class="block mt-1 w-full" type="text" name="title"
                                :value="old('title')" required autofocus />
                            <x-input-error :messages="$errors->get('title')" class="mt-2" />
                        </div>

                        <!-- Password -->
                        <div class="mt-8">
                            <x-input-label for="description" :value="__('Description')" />

                            <x-text-input id="description" class="block mt-1 w-full" type="text" name="description"
                                required />

                            <x-input-error :messages="$errors->get('description')" class="mt-2" />
                        </div>

                        <div class="mt-16">
                            <button type="submit" class="inline-flex items-center px-4 py-2 text-lg text-gray-800 dark:text-gray-200 leading-tight bg-green-500 hover:bg-green-400 rounded-md font-bold transition ease-in-out duration-150">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
